
/**
 * BigInt
 */
const maxSafeNumber = Number.MAX_SAFE_INTEGER;

maxSafeNumber + 1;

maxSafeNumber + 2;


typeof 123;
// → 'number'
typeof 123n;
// → 'bigint'

1234567890123456789n * 123n;

/**
 * Symbol
 */

const mySymbol = Symbol()

Symbol() === Symbol() //false

console.log(Symbol()) //Symbol()
console.log(Symbol('Some Test')) //Symbol(Some Test)


const NAME = Symbol()
const person = {
  [NAME]: 'Flavio'
}

person[NAME] //'Flavio'

const RUN = Symbol()
person[RUN] = () => 'Person is running'
console.log(person[RUN]()) //'Person is running'


/**
 * variable wrapper
 */